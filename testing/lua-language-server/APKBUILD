# Maintainer: psykose <alice@ayaya.dev>
pkgname=lua-language-server
pkgver=3.5.0
pkgrel=0
pkgdesc="Language Server for Lua"
url="https://github.com/sumneko/lua-language-server"
arch="all !s390x !ppc64le" # ftbfs
license="MIT"
makedepends="bash samurai"
source="https://github.com/sumneko/lua-language-server/archive/refs/tags/$pkgver/lua-language-server-$pkgver.tar.gz
	lua-language-server-submodules-$pkgver.zip.noauto::https://github.com/sumneko/lua-language-server/releases/download/$pkgver/lua-language-server-$pkgver-submodules.zip
	wrapper
	"
options="!check" # no tests

prepare() {
	default_prepare

	unzip -o "$srcdir"/lua-language-server-submodules-$pkgver.zip.noauto \
		-d "$builddir"
}

build() {
	ninja -C 3rd/luamake -f compile/ninja/linux.ninja
	./3rd/luamake/luamake rebuild
}

package() {
	install -Dm755 "$srcdir"/wrapper "$pkgdir"/usr/bin/lua-language-server
	install -Dm755 bin/lua-language-server \
		-t "$pkgdir"/usr/lib/lua-language-server/bin
	install -Dm644 bin/main.lua \
		-t "$pkgdir"/usr/lib/lua-language-server/bin
	install -Dm644 debugger.lua main.lua \
		-t "$pkgdir"/usr/lib/lua-language-server
	cp -a locale meta script "$pkgdir"/usr/lib/lua-language-server
}

sha512sums="
e0711c7a456e5861cb4dbf5c3992e874077f8bf2b0e8b09f37e60a560bc6288ad0d001d2e1b59d66e8fafda221238f36ab17eb477d56c15cfbff8cb18a700d10  lua-language-server-3.5.0.tar.gz
f1252042ab87fde1de38c5cc7d7c3f33cfb7193208ae8a82457c02b195527fd5cab753b77433159b3ff5cbf7747720177112b30dbf254523c88d389f2ae86490  lua-language-server-submodules-3.5.0.zip.noauto
9fa9621b61a365a576079731afe419245268b5223292989d2f98091a26b8866ba97a8c6c4cf8e5cbb2704089cb45167630557049430105a71ed4fd55311a543a  wrapper
"
